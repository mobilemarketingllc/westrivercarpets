<?php


// Area Rug Gallery Images
function arearugGallery(){
    $images = array('https://s3-us-west-2.amazonaws.com/rugs.shop/karastan-rug39600_21004_096132_rm01.jpeg',
    'https://s3-us-west-2.amazonaws.com/rugs.shop/karastan_spicemarket_room.jpg',
    'https://uca50882c99ba8a2860406828188.previews.dropboxusercontent.com/p/thumb/AAYrZgEqNjW10QlsL6znTRSpWOTvrXMa8gch4K7a6cSP7GxcC2eTEJVtIF1YLQqQ4MtA17ZDECRdsIqFHPiVbJPMBvtxxMrag_0BFVQdXjJBeF5kX4OabIafJfKInSQ8aCLUePfpex5YgzR8FaoADB16SDDWXdq-m056RHCYlr3pjJwaM_8ydNpvUcLxff1iRxxJBy6Me8Z6OQD8wndk-VlQa-481zCSy6OxNl1FIu3Uqx5nrGsNsWYbcmbZxwWZD8ohzAky4nq7GzKg-dsZfaGLy3lJG4AjAqksCmEAXItc6OPGDy0jcmO7oH8IvziD5MwSZxD07I93IrrahuIb9PJyZ7HyzuRsWhZSo6mCB_B6PXJyU5ryoSu-Bj6t4uk7wkQ/p.jpeg?fv_content=true&size_mode=5',
    'https://uc2e3078cf6c3e0d545bd957cf31.previews.dropboxusercontent.com/p/thumb/AAZE1uy_l2MCT3cYoFn745snlAv2OVTcOQInTt3_Sa064IOp-t24t0wguXQrWg11RY0HxHKihO5Zl9gN98LiLOJqLwR6LQHWYmkPwYSDZWlBRZGQasdkPwBOAB_pIvo-XeyzeaA1KMm3KqMTT85Ma9sMJ9qVNYIvNXy7AOMDUHJePCRa_KhH23jDbVWPxi2ZvFAdhZhitzPZPn8WPdVuixr_dOCe4jE6iLdhMqJk6TFMD0kORizZMK1C9gO4N3TZS7kXZAhKmajOPf2D2kpmG-YzRz2cNXZslRP53-0GQ-j8_YvQlojvSwPf9M5VME95731mD9nnCVKgY9UngYwm7_LCa7HcHsqyDwlDLa1fI_TS-WnAby_24RKH4oWZ7_cXv5o/p.jpeg?fv_content=true&size_mode=5',
    'https://ucba04b34212dce332a9933719e8.previews.dropboxusercontent.com/p/thumb/AAas2RqP6kRooymq7cVEh0uQ1FZKLiGQZAeAEaEZfo3G53bWMj27cQq9-6gFuCTOdN-aTmlPkbH0VIqb7dNT6TeSChXlUn7R2wpMAP7_Qf_wcYG5DRupL19-HWXOlXgtsJGUiND5cZgNqyNFpEsgJvwyn8Gr9sD96G5xwDpfvivGZIm3xfhhLXpVqDB5KPCBSjDgMLkweyt5dAnRdXOINKewYIrM_sQ4pC3pnuDvMWjvKbozCNozeC1l8HzDCun0Rmsf39lhsbZxDtm363OumjIHg7TNXqrU_UfqTASbt0oFU2IzBWYqZ_OMThjoZrDy6eB5E30VE-4wLYw8KWDmqHNKU9mACUK32EMNer3p3KJ50Gc3CWoqTo2CLbpF2vG1MxY/p.jpeg?fv_content=true&size_mode=5',
    'https://ucdc27270fea2babaaac19b88061.previews.dropboxusercontent.com/p/thumb/AAbOn4adlb_8IwURv-llTeymNwOvJWYNAaP0eDKdTFtDwTIoNLCEdA-XRPeKHpJrubOX4c3Z0t_s1nI2opzeKWsrjevVbHisdMOs7pqF0ntBa8bNE0Nc0hDLy0R0BeFXFGphX3VXXezjYZ_c7je8q_60KRiq9a2noTZKaaS_FaP1lAYyq2ohtlH88vjY-sdBAjH2HAfieAnjwRlsVJjtGrB8WUCzj2GOjCKg_9lU_usUQ3BnlbURVJRqQRfDuJCT7OtMWrRpCCjWJkZ1dw59PMpiAdZegMrVkv6-Ij7-X_l7z0o1aEj7Ijbsdzr6dFH7yGTaJwOH0NllL5Id3cGsFN3iLn6TE4rZfFNVOd08DQCAT1JK-5DlVkLl8fhbwlWttIU/p.jpeg?fv_content=true&size_mode=5',
    'https://ucaec0b4b093dcbae9802224b95b.previews.dropboxusercontent.com/p/thumb/AAasCTLUctRx-y2UclCA76Q3V67KjRic917MI19JJ9JSAftfDMh64golgYPJjyaxjpf3TUZ5kO_OikcOH3Lszk0WpfRLbIZcxOPVU05bEKy55ATYlwtHy6wAKuIBs-ho7fSb135-HVL1eRA5TZ8Nu2z92djfBYSbK503EjxfCeIUmwRpKJIa-jtM9941hE3giCEcnCkeWKMK92PWx5t1CvbHbaxUAlCLoaIuZB3EeDZFQ7T3p64OSdqXFUj_W26gnP5Yv34zMe7CIc3axsyVEiWZJB-3VFEwfDmXklQLR2bwtxwxe4dxTqYIXlfArWSHhkACl0IMWeqi6eM-XwYVEE3PCw6jmF5D5tzAQxUnh5_QBH-owh3aLZNr_rGjkt_lOHM/p.jpeg?fv_content=true&size_mode=5',
    'https://uc67211dd60046004e95f15e0495.previews.dropboxusercontent.com/p/thumb/AAZdC9zFI5NgLZAdAChU-wCguOAeO6yNa2lKcljDwWwsgBGdRstrFr7PQuaUUssEm0VFEB05XO5wptwcwEbURIfZIWOE417C5PrXQw6xrKPTIAQ0Br0WkDQItxb4s_u3K-DLWvKWLES_HT4rctxKZMeGBLCu2BPgNBIlxnFG5yTYuZrqoahw9yb3aDbd3Cjk2Cr8nF2YfDc-wvoRkb8cgGkqkvirVeRECWpr5iu16cynEbQ_nm6mOpBxIn42Ox74ZGnRWTvzxJgqA9ogbyO5HbOoxneYrY-wD-CzreQdYTEWMc-qD4R-QUJVyvC418Z2ypXkQomhxhQ7rK9Yqd_DJOpE8fdbi1CH2dWRHaGYZXE1ftGTYYzus2IjzYsyP6akwEM/p.jpeg?fv_content=true&size_mode=5',
    'https://uca876d78c774cebc7e8a922bd2f.previews.dropboxusercontent.com/p/thumb/AAYBSM1QFcYvAoEYKIZfwg4ELQhz9k6bq81Ucdixcym-zeAAiQ3__Kl0rAQAtHzzClrD9UDn-UGh53bf_9hy8EVWV6uq-z1rDQL58UKng4i_bio-X0TqRdPQMhqDGpDkOSFqwhYm2KIIkGsKM_PbSkF2uf40Vn-PzrKjIxVQWgkNPoNQaFW9WL2hiPHo4If9Rm2G9Km75mCs0ew1liW1t1fiN8_Pe54l-VjkMOpPkntKXI3aQJsc2ho0d3L7aa2cAL4ADBRlUtweiYgyqVWlh8NW-UC524SqKBFDUpWAnxfwc5ZvYQNeOCx27BwXhxX-9gfqYC0suI0tennLIplK6P8d3qDSIPluQHfShtmXM0YiZvMXXyY4TojpbW3S2cx6SZM/p.jpeg?fv_content=true&size_mode=5',
    'https://ucc2edb556adee3cdcab67e2fd3a.previews.dropboxusercontent.com/p/thumb/AAbIJ8tW2XcmxqiSJZ9BVk8n7s9cLf7W6PXi0xW8uLnzWk0rx_HShpmvs9r7es4ec2irQocLsxP1-MRVngLJEPTv1F-Ng9PQ9fzWKCs-OLKvTmqiXBqpC0F-1tyg6-hmIDtfvS1dfGQ4Em5Prl6c2rulAuRVW7wbshgd79sMAsireyYYJnSFpqVnwKXgNwluUSp9wj0jUsT3r-JeueYunJNNvChC0zq6RmR2R4cIrYV6VhZq5QqjAG04S1GM_tVapvTLH_VhCkFhIaDrwJPu0gt4-uXcM2Ooo-wGoeYRqhFRH7WkrO7rbiH6rAAgzXLfIJdiNLQHffpSwWYxZ9wgKA0XTONFR2M2JhQ44nAae0xDCU4w66ML_5CRL2kMrmrtvBc/p.jpeg?fv_content=true&size_mode=5',
    'https://uc3bb3105e96b307b6e8af595264.previews.dropboxusercontent.com/p/thumb/AAYryib-9JHRSzDKOA542-KtBQwyzxF47AKx0-9YXfg7VRd21rCDhtF3aoevV6QQguBtVO4Ld_LA73R4nXD38w1bAvPxMzMCZKjLJOmlISFjLrqzB8EOLyLrDS0jp9QNS4QZeompNQlg2-CM3qZn3KfGH_fpi_Td5i8Lb8IPdazAfctxhyP-yxX5uXjwMCFNE-NnIWEw1TD_SHW1LceKZdQ4H-wRX4zckpBcFN4bNs7Ic3k9pk_lJUhq7x5hxvgfutUrWns1qrnDArRvrWigphTjU0bnBO7wZ1GLTfCoBq60BnLjCcKGEWo1M7CEAedULERnkd3Av8-BxA-ji6OtQ34_yiYwG58tV_aLKmS24Aghc-H3foGVf744JvoXgSJnOUI/p.jpeg?fv_content=true&size_mode=5');

$result = '<div id="lightgallery" class="rug-gallery">';
 foreach ($images as $img) {
     $result .= '<a href="'.$img.'"><img src="'.$img.'" /></a>';
 }
    $result .= '</div>';
    return $result;
}
add_shortcode('areagallery', 'arearugGallery');
//Tranding Product ShortCode

function shopofarearug($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-colors/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Color.png', __FILE__),
            'title'=> "Shop by Color"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-styles/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/style.png', __FILE__),
            'title'=> "Shop by Style"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-sizes/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Size.png', __FILE__),
            'title'=> "Shop by Size"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-patterns/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/patterns.png', __FILE__),
            'title'=> "Shop by Patterns"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-brands/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/brands.png', __FILE__),
            'title'=> "Shop by Brands"
        ),
        array(
            'url'=>'https://rugs.shop/on-sale/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/sale.png', __FILE__),
            'title'=> "Area Rug Sale"
        ),
       
    );
    $result ="";
    $result.= '<div class="products-list column-3"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="'.$data[$i]['url'].'" target="_blank" itemprop="url" class="">
                                <img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h3>
                    </div>
                </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('shoparearug', 'shopofarearug');

//Tranding Product ShortCode

function arearugProductList($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'99.00',
            'title'=> "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'499.00',
            'title'=> "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'156.00',
            'title'=> "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'62.40',
            'title'=> "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result ="";
    $result.= '<div class="products-list"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="'.$data[$i]['url'].'" target="_blank"><img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" /></a>
                </div>
                <div class="product-info">
                    <h6>'.$data[$i]['collection'].'</h6>
                    <h4><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $'.$data[$i]['price'].'</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="'.$data[$i]['url'].'" class="button" target="_blank">Buy Now</a>
                        <div class="brand-wrap">By '.$data[$i]['brand'].'</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('area_rug_trading_products', 'arearugProductList');



/* function contact_locationlist($arg)
{
    return '<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
            <div class="">
                <div class="fl-icon-wrap">
                    <div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
                        <a href="'.get_option('api_contact_url_sandbox').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">
                            <p>'.get_option('api_contact_address_sandbox').'</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>';
}
add_shortcode('contactLocation', 'contact_locationlist'); */

function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}

function storelocation_address($arg)
{
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'>";
    $locations .= '<li>';
    
    
    for ($i=0;$i<count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";
        if (in_array(trim($location_name), $arg)){

            $location_address_url =  isset($website->locations[$i]->name)?$website->locations[$i]->name." ":"";
            $location_address_url  .= isset($website->locations[$i]->address)?$website->locations[$i]->address." ":"";
            $location_address_url .= isset($website->locations[$i]->city)?$website->locations[$i]->city." ":"";
            $location_address_url .= isset($website->locations[$i]->state)?$website->locations[$i]->state." ":"";
            $location_address_url .= isset($website->locations[$i]->postalCode)?$website->locations[$i]->postalCode." ":"";
    
            $location_address  = isset($website->locations[$i]->address)?"<p>".$website->locations[$i]->address."</p>":"";
            $location_address .= isset($website->locations[$i]->city)?"<p>".$website->locations[$i]->city.", ":"<p>";
            $location_address .= isset($website->locations[$i]->state)?$website->locations[$i]->state:"";
            $location_address .= isset($website->locations[$i]->postalCode)?" ".$website->locations[$i]->postalCode."</p>":"</p>";
            
            $location_phone  = "";
            if (isset($website->locations[$i]->phone)) {
                $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
            }
            if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                
            }
            else{
    
                $forwarding_tel ="#";
                $forwarding_phone  = formatPhoneNumber(8888888888);
            }


            if (in_array("loc", $arg)) {

                if (in_array('nolink', $arg)){
                    $locations .= '<div class="store-container">';
                    //$locations .= '<div class="name">'.$location_name.'</div>';
                    $locations .= '<div class="address">'.$location_address.'</div>';
                    //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                    $locations .= '</div>';
                }else{

                    $locations .= '<div class="store-container">';
                    //$locations .= '<div class="name">'.$location_name.'</div>';
                    $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" '.'>'.$location_address.'</a></div>';
                    //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                    $locations .= '</div>';

                }
                    
                
            }
            if (in_array("forwardingphone", $arg)) {

                    if (in_array('nolink', $arg)){
                        $locations .= "<div class='phone'><span>".$forwarding_phone."</span></div>";
                    }
                    else{
                        $locations .= "<div class='phone'><a href='tel:".$forwarding_tel."'><span>".$forwarding_phone."</span></a></div>";
                    }

            }
            if (in_array("ohrs", $arg)) {
                    $locations .= '<div class="store-opening-hrs-container">';
                    $locations .= '<ul class="store-opening-hrs">';
                        
                    for ($j = 0; $j < count($weekdays); $j++) {
                        $location .= $website->locations[$i]->monday;
                        if (isset($website->locations[$i]->{$weekdays[$j]})) {
                            $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                        }
                    }
                    $locations .= '</ul>';
                    $locations .= '</div>';
            }
            if (in_array("dir", $arg)) {
                
                    $locations .= '<div class="direction">';
                    $locations .= '<a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                                        <span class="button-text">GET DIRECTIONS</span></a>';
                    $locations .= '</div>';
                
            }
            if (in_array("map", $arg)) {
                
                    $locations .= '<div class="map-container">
                    <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                    </div>';
                
            }
            if (in_array("phone", $arg)) {
                        $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
            }
        } 
        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
       
        
        
        


        /* if (in_array("alldata", $arg)) {
            $locations .= '<div class="store-container">';
            //$locations .= '<div class="name">'.$location_name.'</div>';
            $locations .= '<div class="address"><a href=https://www.google.com/maps/dir//'.urlencode($location_address_url).' target="_blank" '.'>'.$location_address.'</a></div>';
            //  $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
            $locations .= '</div>';

            $locations .= '<div class="store-opening-hrs-container">';
            $locations .= '<ul class="store-opening-hrs">';
            for ($j = 0; $j < count($weekdays); $j++) {
                $location .= $website->locations[$i]->monday;
                if (isset($website->locations[$i]->{$weekdays[$j]})) {
                    $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                }
            }
            $locations .= '</ul>';
            $locations .= '</div>';
            $locations .= '<div class="direction">';
            $locations .= '<a href="https://www.google.com/maps/dir//'.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                            <span class="button-text">GET DIRECTIONS</span></a>';
            $locations .= '</div>';
            $locations .= '<div class="map-container">
            <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
        }  */
        $locations .= '</li>';
    }
    $locations .= '</ul>';
    
    return $locations;
}
    add_shortcode('storelocation_address', 'storelocation_address');

 /*    function storelocation_map($arg)
    {
        $website = json_decode(get_option('website_json'));
        
        for ($i=0;$i<count($website->locations);$i++) {
            $map = '<div class="map-container">
                <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.$website->locations[$i]->address.$website->locations[$i]->city.$website->locations[$i]->postalCode.$website->locations[$i]->state.'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
            return $map;
        }
    }
        add_shortcode('storelocation_map', 'storelocation_map');
 */
function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


/* function contact_address_display($atts)
{
    return '
    <div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
    <div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d734">
							<i class="fas fa-map-marker" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
				<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
				<p>'.get_option('api_contact_address_sandbox').'</p>				</a>
			</div>
</div>
</div></div>
<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
		<div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="tel:'.get_option('api_contact_phone').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d770">
							<i class="fas fa-phone" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d770" class="fl-icon-text">
				<a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
</a><p><a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap"></a><a href="tel:'.get_option('api_contact_phone').'">'.get_option('api_contact_phone').'</a></p>
			</div>
	
</div>
	</div>
</div>'
;
}
add_shortcode('contactAddress', 'contact_address_display');

https://www.google.com/maps/dir//Dalton+Wholesale+Floors,+411+Soho+Dr,+Adairsville,+GA+30103/@34.381066,-84.9075907,17z/data=!4m15!1m6!3m5!1s0x88f555733e5d7859:0x4a06216b9216e888!2sDalton+Wholesale+Floors!8m2!3d34.381066!4d-84.905402!4m7!1m0!1m5!1m1!1s0x88f555733e5d7859:0x4a06216b9216e888!2m2!1d-84.905402!2d34.381066
function getDirectionButton()
{
    return '
		<div class="getdirection">
			<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-button" role="button">
							<span class="fl-button-text">GET DIRECTIONS</span>
                    </a>
                    
</div>
	';
}
add_shortcode('getDirection', 'getDirectionButton');

function getOpeningHours()
{
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('api_opening_hr_fields_sandbox').'
                    
</div>
	';
}
add_shortcode('getOpeningHours', 'getOpeningHours'); */

function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->platform=="googleBusiness") {
                $value->platform = "google-plus";
            }
            if ($value->platform=="linkedIn") {
                $value->platform = "linkedin";
            }
            
                
                
            if ($value->active) {
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.strtolower($value->platform).'"></i></a></li>';
            }
        }
        $return  .= '</ul>';
    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');

function create_post_type()
{
    register_post_type(
        'store-locations',
        array(
        'labels' => array(
          'name' => __('Store Locations'),
          'singular_name' => __('Store Location')
        ),
        'public' => true,
        'has_archive' => true,
      )
    );
}
add_action('init', 'create_post_type');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info="";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));
    
    for ($i=0;$i<count($website->locations);$i++) {
        if (in_array($website->locations[$i]->name, $atts)) {
            if (in_array("license", $atts)) {
                $info  .= "<div class='store-license'>".$website->locations[$i]->licenseNumber."</div>";
            }
            if (in_array("phone", $atts)) {
                $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $website->locations[$i]->phone)."'>".formatPhoneNumber($website->locations[$i]->phone)."</a>";
            }
        }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings() {

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId =$sharpspringTrackingId ="";
    for($j=0;$j<count($website_json->sites);$j++){
        
        if($website_json->sites[$j]->instance == ENV){
           
                $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
                $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
           
            }
        }
        
       if(isset($sharpspringTrackingId) && trim($sharpspringTrackingId) !="" ){
   
    ?>
           <script type="text/javascript">

                    var _ss = _ss || [];

                    _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId;?>.marketingautomation.services/net']);

                    _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId;?>']);

                    _ss.push(['_trackPageView']);

                    (function() {

                    var ss = document.createElement('script');

                    ss.type = 'text/javascript'; ss.async = true;

                    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId;?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                    var scr = document.getElementsByTagName('script')[0];

                    scr.parentNode.insertBefore(ss, scr);

                    })();

                    </script>
    <?php
       }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg){


    $website = json_decode(get_option('website_json'));
    if (in_array('city', $arg)){
        $info =  isset($website->locations[0]->city)?"<span class='city retailer'>".$website->locations[0]->city."</span>":"";
    }
    else if (in_array('state', $arg)){
        $info =  isset($website->locations[0]->state)?"<span class='state retailer'>".$website->locations[0]->state."</span>":"";
    }
    else if (in_array('zipcode', $arg)){
        $info =  isset($website->locations[0]->postalCode)?"<span class='zipcode retailer'>".$website->locations[0]->postalCode."</span>":"";
    }
    else if (in_array('legalname', $arg)){
        $info =  isset($website->name)?"<span class='name retailer'>".$website->name."</span>":"";
    }
    else if (in_array('address', $arg)){
        $info =  isset($website->locations[0]->address)?"<span class='street_address retailer'>".$website->locations[0]->address."</span>":"";
    }
    else if (in_array('phone', $arg)){
        if (isset($website->locations[0]->phone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->phone);
            $location_phone  = formatPhoneNumber($website->locations[0]->phone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->phone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->phone)?"<a href='tel:".$location_tel."' class='phone retailer'>"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('forwarding_phone', $arg)){
        if (isset($website->locations[0]->forwardingPhone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->forwardingPhone);
            $location_phone  = formatPhoneNumber($website->locations[0]->forwardingPhone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->forwardingPhone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->forwardingPhone)?"<a href='tel:".$location_tel."' class='phone retailer' >"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('companyname', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->name)?"<span class='companyname retailer'>".$website->sites[$j]->name."</span>":"";
                    break;
                }
            }
    }
    else if (in_array('site_url', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->url)?"<span class='site_url retailer'>".$website->sites[$j]->url."</span>":"";
                    break;
                }
            }
    }
    
    return $info;  
}

add_shortcode('Retailer', 'getRetailerInfo');


function updateSaleCoupanInformation($arg){
    $promos = json_decode(get_option('promos_json'));

    
    /* foreach($promos as $promo){
        
        foreach($promo->widgets as $widget){
            
            
            if ($widget->type == "message") {
                $i =0;
                $seleinformation['message_link'] =$widget->link;
                foreach($widget->texts as $text ){
                    if($text->type =="copy"){
                        $seleinformation['message'] =$text->text;
                     //   var_dump($seleinformation);
                    }
                    
                    
                }
                $i++;
            }
            if ($widget->type == "navigation") {
                $i =0;
                $seleinformation['navigation_link'] =$widget->link;
                foreach($widget->images as $slide){

                    if($slide->type == "graphic"){
                      //  $slider['slider_coupan_img'] =  $slide->url;
                      $seleinformation['navigation_img'] ="https://".$slide->url;
                    }
                }
                $i++;
            }
            
            if ($widget->type == "banner") {
                $i =0;
                $seleinformation['banner_link'] =$widget->link;
                foreach($widget->images as $slide){
                    
                    if($slide->type == "mobile"){
                      //  $slider['slider_coupan_img'] =  $slide->url;
                      $seleinformation['banner_img_mobile'] ="https://".$slide->url;
                    }
                    if($slide->type == "desktop"){
                      //  $slider['slider_bg_img'] =  $slide->url;
                      $seleinformation['banner_img_deskop'] ="https://".$slide->url;
                      
                    }
                }
                $i++;
            }
            if($widget->type == "slide" ){
                $i =0;
                $seleinformation['slide_link'] =$widget->link;
                foreach($widget->images as $slide){

                    if($slide->type == "graphic"){
                      //  $slider['slider_coupan_img'] =  $slide->url;
                      $seleinformation['slider'][$i]['slider_coupan_img'] ="https://".$slide->url;
                      $seleinformation['image_onform'] ="https://".$slide->url;
                       $seleinformation['slider'][$i]['link'] =$widget->link;
                    }
                    if($slide->type == "background"){
                      //  $slider['slider_bg_img'] =  $slide->url;
                      $seleinformation['slider'][$i]['slider_bg_img'] ="https://".$slide->url;
                      $seleinformation['background_image'] ="https://".$slide->url;
                      
                    }
                }
                $i++;
                
            }
            if($widget->type == "landing" ){
                $seleinformation['landing_link'] =$widget->link;
                foreach($widget->images as $slide){
                    
                    if($slide->type == "graphic"){
                        $seleinformation['slider_coupan_img'] = "https://".$slide->url;
                        $seleinformation['image_onform'] ="https://".$slide->url;
                        
                    }
                  
                }

                foreach($widget->texts as $text ){
                    if($text->type =="copy"){
                        $seleinformation['content'] =$text->text;
                     //   var_dump($seleinformation);
                    }
                    if($text->type =="heading"){
                        $seleinformation['saveupto_doller'] =$text->text;
                       
                    }
                    
                    
                }
                
            }

            
           
        }//Widget
        
    } */
  //  var_dump($seleinformation);
//    exit;
    //$seleinformation['saveupto_doller'] =
    /* $seleinformation =array(
        'banner_image' =>"https://premierefloorsdc-stg.mm-dev.agency/wp-content/uploads/2019/04/FullWidthBanner_1440X100.jpg",
        'saveupto_doller' =>"$4400",
        'content' =>"<p>Spring is in the air! It is the perfect time to refresh your space, and we’re excited to help you upgrade your flooring. During our Refresh Your Space Sale from April 12<sup>th</sup> through May 31<sup>st</sup>, save up to $1,000* on select Shaw Flooring styles. Simply fill out the form and we’ll email you your coupon to save!</p>
        <p>*Ask your salesperson for details and qualifying styles. Coupon must be presented to a flooring representative at time of purchase, not after transaction. This coupon entitles you to receive 10% off any qualifying flooring purchase from select Shaw Flooring brands between $5,000 - $10,000, for a maximum savings of up to $1,000. In-store only, no online sales. Discount applies to flooring material ONLY, excluding trim, pad and underlayment. This coupon cannot be combined with any other coupon, sale or offer. Coupon is non-transferable and may not be duplicated or used more than once. Returns of any portion of the purchase will require equal forfeiture of offer or amount equal to offer. One coupon limit per household. This coupon is not applicable to previous and/or open orders. Coupon has no cash value. Consumer must pay any sales tax.</p>
        <p>Offer void where prohibited or restricted by law. Offer good only in the USA and Canada. Special financing available at participating retailers pending credit approval on your purchase of any qualifying flooring products at our store. Offer subject to change or cancellation in whole or in part without notice. Offer cannot be combined with any other manufacturer’s offer. All offers may be audited, and unsubstantiated offers may be denied. Fraudulent submissions of multiple requests could result in prosecution. Void where taxed, prohibited or restricted by law. Offer is available only to retail purchasers of our store. Manufacturers, distributors, and dealers of our store products and their families may not participate in this sale.</p>
        <p>Coupon Valid: 4/12/19 - 5/31/2019</p>",
        'image_onform'=>"https://premierefloorsdc-stg.mm-dev.agency/wp-content/uploads/2019/04/Coupon_LP_Graphic_1200x1200.jpg",
        'background_image' =>"https://premierefloorsdc-stg.mm-dev.agency/wp-content/uploads/2019/04/Coupon_LP_Graphic_1200x1200.jpg"
    
    ); */
    //$seleinformation = json_decode('{"banner_image":"https:\/\/premierefloorsdc-stg.mm-dev.agency\/wp-content\/uploads\/2019\/04\/FullWidthBanner_1440X100.jpg","saveupto_doller":"$4400","content":"<p>Spring is in the air! It is the perfect time to refresh your space, and we\u2019re excited to help you upgrade your flooring. During our Refresh Your Space Sale from April 12<sup>th<\/sup> through May 31<sup>st<\/sup>, save up to $1,000* on select Shaw Flooring styles. Simply fill out the form and we\u2019ll email you your coupon to save!<\/p>\n        <p>*Ask your salesperson for details and qualifying styles. Coupon must be presented to a flooring representative at time of purchase, not after transaction. This coupon entitles you to receive 10% off any qualifying flooring purchase from select Shaw Flooring brands between $5,000 - $10,000, for a maximum savings of up to $1,000. In-store only, no online sales. Discount applies to flooring material ONLY, excluding trim, pad and underlayment. This coupon cannot be combined with any other coupon, sale or offer. Coupon is non-transferable and may not be duplicated or used more than once. Returns of any portion of the purchase will require equal forfeiture of offer or amount equal to offer. One coupon limit per household. This coupon is not applicable to previous and\/or open orders. Coupon has no cash value. Consumer must pay any sales tax.<\/p>\n        <p>Offer void where prohibited or restricted by law. Offer good only in the USA and Canada. Special financing available at participating retailers pending credit approval on your purchase of any qualifying flooring products at our store. Offer subject to change or cancellation in whole or in part without notice. Offer cannot be combined with any other manufacturer\u2019s offer. All offers may be audited, and unsubstantiated offers may be denied. Fraudulent submissions of multiple requests could result in prosecution. Void where taxed, prohibited or restricted by law. Offer is available only to retail purchasers of our store. Manufacturers, distributors, and dealers of our store products and their families may not participate in this sale.<\/p>\n        <p>Coupon Valid: 4\/12\/19 - 5\/31\/2019<\/p>","image_onform":"https:\/\/premierefloorsdc-stg.mm-dev.agency\/wp-content\/uploads\/2019\/04\/Coupon_LP_Graphic_1200x1200.jpg","background_image":"https:\/\/premierefloorsdc-stg.mm-dev.agency\/wp-content\/uploads\/2019\/04\/Coupon_LP_Graphic_1200x1200.jpg"}');
    $seleinformation = json_decode(get_option('saleinformation'));
    //update_option('saleinformation',json_encode($seleinformation));
    $seleinformation = (object)$seleinformation;
    
    if(in_array('salebanner',$arg)){
        if(isset($seleinformation->banner_img_deskop)&& $seleinformation->banner_img_deskop != "")
        $div = "<div class='salebanner banner-deskop'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->banner_img_deskop."' alt='sales' /></a></div>";
        if(isset($seleinformation->banner_img_mobile)&& $seleinformation->banner_img_mobile != "")
        $div .= "<div class='salebanner banner-mobile'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->banner_img_mobile."' alt='sales' /></a></div>";
    }
    if(in_array('categorybanner',$arg)){
     
        if(isset($seleinformation->category_banner_img_deskop)&& $seleinformation->category_banner_img_deskop != ""){ 
        $div = "<div class='salebanner ts banner-deskop'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_deskop."' alt='sales' /></a></div>";        
        }
        if(isset($seleinformation->category_banner_img_mobile)&& $seleinformation->category_banner_img_mobile != "")
        $div .= "<div class='salebanner banner-mobile'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_mobile."' alt='sales' /></a></div>";
    }
     if(in_array('heading',$arg)){
        $price = isset($seleinformation->saveupto_doller)?$seleinformation->saveupto_doller:"";
        if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
            
            $div ='<h1 class="googlekeyword">'.$price;
        } else {
            $keyword = $_COOKIE['keyword'];
            $brand = $_COOKIE['brand'];
            $div = '<h1 class="googlekeyword">'.$price.' on '.$brand.' '.$keyword.'<h1>';
        }
    }
     if(in_array('content',$arg)){
        
         $string = isset($seleinformation->content)?$seleinformation->content:"";
         $div = '<div class="form-content">'.$string.'</div>';
    }
    if(in_array('image_onform',$arg) && isset($seleinformation->image_onform)){
        
        if(in_array('backgrondimage',$arg) || in_array('background_img',$arg)){

            $div = "<div class='salebanner floor-coupon-img  banner-deskop'  id='backgrondimage' ><img src='".$seleinformation->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'  id='backgrondimage' ><img src='".$seleinformation->image_onform_mobile."' alt='sales' /></div>";
        }
        else    {
            $div = "<div class='salebanner floor-coupon-img  banner-deskop'><img src='".$seleinformation->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'><img src='".$seleinformation->image_onform_mobile."' alt='sales' /></div>";
        }
    }
    if(in_array('message',$arg) && isset($seleinformation->message)){
        
        $div = "<div class='message'>".$seleinformation->message."</div>";
       
    }
    if(in_array('navigation',$arg) && isset($seleinformation->navigation_img)){
        if (in_array('image', $arg)) {
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'><img src='".$seleinformation->navigation_img."' alt='sales' /></a></div>";
        }
        else if(in_array('text',$arg)){
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'>'".$seleinformation->navigation_text."</a></div>";
        }
       
    }
    if (in_array('homepage_banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                $div = "<div class='floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
	if (in_array('banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                $div = "<div class='banner-image' style=''><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
    
    
    
    return $div;
}

add_shortcode('coupon', 'updateSaleCoupanInformation');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©".do_shortcode('[fl_year]')." ".get_bloginfo()." All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');

